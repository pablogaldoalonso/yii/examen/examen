<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionConsulta1dao(){
   
    
    $dataProvider = new SqlDataProvider([
        'sql'=>' SELECT distinct nombre, ciclista.dorsal FROM etapa INNER JOIN ciclista USING (dorsal) INNER JOIN lleva USING (numetapa) LEFT JOIN maillot USING (código) WHERE maillot.color NOT LIKE ("rosa")',
       
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['nombre' , 'dorsal'],
        "titulo"=>"Consulta Examen Dao",
        "enunciado"=>"Los nombres y dorsales de los ciclistas que habiendo ganado etapas, nunca han llevado el maillo rosa",
        "sql" =>"SELECT distinct nombre, ciclista.dorsal FROM etapa INNER JOIN ciclista USING (dorsal) INNER JOIN lleva USING (numetapa) LEFT JOIN maillot USING (código) WHERE maillot.color NOT LIKE ('rosa');",
       "texto" => "Uno las tablas por los puntos que me interesan para, tener por un lado los nombres y por otro los colores de maillot, luego con un not like descarto los rosas",
    ]);
        
    }
    
    public function actionConsulta1orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Etapa::find()->select ("sum(kms) as kilmetros_recorridos"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['kilmetros_recorridos'],
            "titulo"=>"Consulta examen Active Record",
            "enunciado"=>"Número total de kms recorridos por los ciclistas  ",
            "sql"=>"SELECT  COUNT(*) as Kilmetros_recorridos FROM etapa",
            "texto" => "Sumo de la tabla etapa los kilometros, y le doy un Alias para ello",
             
            
        ]); 
    }


    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
